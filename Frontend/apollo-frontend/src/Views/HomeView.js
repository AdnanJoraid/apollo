/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "../Styles/HomeView.css";
import doubleas from "../Styles/images/doubleas.png";

const HomeView = () => {
  const navigate = useNavigate();
  return (
    <body>
      <div class="navbar">
        <img alt="background" src={doubleas} class="logo" />
        <ul>
          <li>
            <a>Home</a>
          </li>
          <li>
            <a>Player</a>
          </li>
          <li>
            <a>Organization</a>
          </li>
          <li>
            <a>Team</a>
          </li>
          <li>
            <a>About</a>
          </li>
        </ul>
      </div>
      <div className="content">
        <h1>Apollo</h1>
        <p>Start By Choosing Your Option</p>
        <div>
          <button type="button" onClick={() => navigate("/TeamHome")}>
            <span></span> <a class="link"> Team</a>
          </button>

          <button type="button" onClick={() => navigate("/UserHome")}>
            {" "}
            <span></span>
            <a className="link">Player</a>
          </button>

          <button type="button" onClick={() => navigate("/OrganizationHome")}>
            {" "}
            <span></span>
            <a className="link">Organization</a>
          </button>
        </div>
      </div>

      <div className="footer">
        <p>© 2022 by Double A's - Sheridan College</p>
      </div>
    </body>
  );
};

export default HomeView;
