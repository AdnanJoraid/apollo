import React, { useState } from "react";
import "../../Styles/HomeView.css";
import { useNavigate } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";

const OrganizationAddPlayer = () => {
  const navigate = useNavigate();

  const [getName, setName] = useState("")
  const [getType, setType] = useState("")

  const [addActivity, setActivity] = useState({
    activityName: "",
    activityType: "",
    org: {
      name: "",
    },
    players: [],
  });

  const handleSubmit = () => {

    axios
      .post("http://localhost:5000/api/organization/addplayer", {
          name: getName, 
          type: getType, 
          org: {
              name : null 
          }, 
          players : []
      })
      .then((response) => {
        console.log(response);
        //event.preventDefault();

      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response);
          console.log("server responded");
        } else if (error.request) {
          console.log("network error");
        } else {
          console.log(error);
        }
      });
  };

  return (
    <div className="content">
      <h1>Organization Add Player Activity</h1>
      <div id="cent">
        <form >
          <label>
            Activity Name:
            <input
              type="text"
              name="activityName"
              onChange={(e) =>  setName(e.target.value)}
            />
          </label>
          <br />

          <label>
            Activity Type:
            <input
              type="text"
              name="activityType"
              onChange={(e) => setType(e.target.value)}
            />
          </label>
          <br />

          <Button
            type="submit"
            style={{ marginLeft: 85, backgroundColor: "black" }}
            onClick={(e) => {handleSubmit(); e.preventDefault()}}
          >
            {" "}
            Add Activity
          </Button>
        </form>

        {/* <Form>
          <Form.Group className="mb-3" controlId="{exampleForm.ControlInput1}">
            <Form.Label>Inventory Item ID</Form.Label>
            <Form.Control
              type="input"
              placeholder="Enter Inevntory Item ID"
              required
              onChange={(e) => console.log(e.target.value)}
            ></Form.Control>
          </Form.Group>
        </Form> */}
      </div>
    </div>
  );
};

export default OrganizationAddPlayer;
