import React from "react";
import { useNavigate } from "react-router-dom";
import "../../Styles/HomeView.css";

const OrganizationHome = () => {
  const navigate = useNavigate();

  return (
    <div class="content">
      <h1>Organization Home</h1>
      <button type="button" onClick={() => navigate("/organizationAddPlayer")}>
        <span></span> <a class="link">Add Player Activity </a>
      </button>

      <button onClick={() => navigate("/organizationAddTeam")}>
        <span></span> <a class="link"> Add Team Activity</a>
      </button>
    </div>
  );
};

export default OrganizationHome;

