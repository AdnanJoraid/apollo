import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import "../../Styles/HomeView.css";
import axios from "axios";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

const UserActivityListView = ({ playerName }) => {
  const navigate = useNavigate();
  // const response = await axios.get(http://localhost:5000/api/player/getActivites)

  // const req = async () => {
  //   axios.get('http://localhost:5000/api/player/getActivites').then((response) =>{
  //     console.log(response)
  //   })

  // }
  // req()

  const [getActivities, setActivites] = useState([]);

  useEffect(() => {
    getAllPlayerActivities();
  }, []);

  const getAllPlayerActivities = () => {
    axios
      .get("http://localhost:5000/api/player/getActivites")
      .then((response) => {
        setActivites(response.data);
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response);
          console.log("server responded");
        } else if (error.request) {
          console.log("network error");
        } else {
          console.log(error);
        }
      });
  };

  const addPlayerToActivity = (activityId) => {
    console.log(activityId);
    axios({
      method: "post",
      headers: { "Content-Type": "application/json" },
      url: `http://localhost:5000/api/player/${activityId}/addplayer`,
      data: {
        id:"66973a83-2e72-416e-a4f9-d11cc4c28b34",
        name:"BILL"
      },
    })
      .then((res) => console.log(res))
      .catch((err) => console.error(err));
  };

  return (
    // <div className="center">
    //   <h2>User Activity List</h2>

    //   <ul>
    //     <li style={{ marginBottom: "20px" }}>
    //       <div>Soccer Activity</div>
    //       <div>Time: Today 7pm</div>
    //       <button onClick={() => navigate(`/userJoinActivity`,{state:{sport:"Soccer"}}) }>
    //         Join Activity
    //       </button>
    //     </li>

    //     <li>
    //       <div>Basketball Avitiy </div>
    //       <div>Time: Tuesday May 17th 7pm</div>
    //       <button onClick={() => navigate(`/userJoinActivity`,{state:{sport:"Basketball"}})}>
    //         Join Activity
    //       </button>
    //     </li>
    //   </ul>
    // </div>
    <div className="content">
      <h1>Player Activity View</h1>

      <div style={{ overflow: "scroll" }} id="cent">
        <div style={{ alignItems: "center" }}>
          <Table style={{ marginLeft: "auto", marginRight: "auto" }}>
            <thead>
              <tr>
                <th>Activity Name</th>
                <th>Activty Type</th>
                <th>Join</th>
              </tr>
            </thead>
            <tbody>
              {getActivities.map((activity, idx) => (
                <tr key={idx + 1}>
                  <td>{activity?.name}</td>
                  <td>{activity?.type}</td>
                  <td>
                    <Button
                      onClick={() => {
                        addPlayerToActivity(activity?.id);
                      }}
                      style={{ background: "black", width: 50 }}
                    >
                      Join
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default UserActivityListView;
