import React from "react";
import { useNavigate } from "react-router-dom";
import "../../Styles/HomeView.css";


const UserHomeView = () => {

    const navigate = useNavigate();

    return(
        <div className="center">
            <h2>User HomePage</h2>
            <button
            onClick={() => navigate("/userActivityList")}
            >
                Find Activities
            </button>

            <button
            onClick={() => navigate("/userChat")}
            >
                Message chat
            </button>
        </div>
    

    );
};
export default UserHomeView;