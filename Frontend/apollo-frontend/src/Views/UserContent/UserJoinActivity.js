import React from "react";
import { useNavigate, useLocation} from "react-router-dom";
import "../../Styles/HomeView.css";

const UserJoinActivity = () => {
  const navigate = useNavigate();
  const location = useLocation();


  return (
    <div className="center">
      <h2>User Join Activity</h2>
      <h1>{location.state.sport}</h1>
    </div>
  );
};

export default UserJoinActivity;
