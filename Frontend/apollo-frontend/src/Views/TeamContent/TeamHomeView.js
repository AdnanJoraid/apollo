import React from "react";
import "../../Styles/HomeView.css"
import { useNavigate } from "react-router-dom";

const TeamHomeView = () => {
    const navigate = useNavigate()
    return (
      <div class="content">
        <h1>Team Home</h1>
        <button onClick={() => navigate("/FindPlayer")}>
          <span></span> <a className="link"> Find/Add a Player to Team</a>
        </button>
        {/* <button type="button" onClick={() => navigate("/TeamChat")}>
          <span></span> <a class="link">Chat With Team </a>
        </button> */}

        <button type="button" onClick={() => navigate("/BookTeamActivity")}>
          <span></span> <a className="link">Play Against a Team </a>
        </button>
      </div>
    );
}

export default TeamHomeView