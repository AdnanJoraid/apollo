import { Alert } from "bootstrap";
import React, {useState} from "react";
import Button from "react-bootstrap/Button";

const BookActivityView = () => { 
  const [getAgainst, setAgainst] = useState("") 
   const [getType, setType] = useState("") 
  return (
    <div className="content">
      <h1>Playing Against Team</h1>
      <div id="cent">
        <form onSubmit={(e) => e.target.submit()}>
          <label>
            Playing Against Team:
            <input
              type="text"
              name="activityName"
              onChange={(e) => setAgainst(e.target.value)}
            />
          </label>
          <br />

          <label>
            Activity Type:
            <input
              type="text"
              name="activityType"
              onChange={(e) => setType(e.target.value)}
            />
          </label>
          <br />
          <Button
            type="submit"
            style={{ marginLeft: 85, backgroundColor: "black" }}
            onClick={(e) => {
              alert(`Team Activity has been booked against ${getAgainst} with Activity Type ${getType}`);
              e.preventDefault();
            }}
          >
            Add Team Activity
          </Button>
        </form>
      </div>
    </div>
  );
};

export default BookActivityView
