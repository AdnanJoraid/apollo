import React, { useState } from "react";
import "../../Styles/HomeView.css";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import axios from "axios";

const FindPlayerView = () => {
  const [getPlayer, setPlayer] = useState();
  const [getSearchedPlayerName, setSearchedPlayerName] = useState("");

  const searchPlayer = () => {
    axios
      .get(
        `http://localhost:5000/api/player/getplayer/${getSearchedPlayerName}`
      )
      .then((res) => {
        setPlayer(res.data);
        console.log(getPlayer);
      })
      .catch((e) => console.log(e));
  };

  const addToTeam = () => {
    console.log(getPlayer)
    axios({
      method : "post", 
      headers : { "Content-Type": "application/json"},
      url : "http://localhost:5000/api/team/addplayer/12276B3F-2234-4F68-9715-7B280A787F68",
      data: {
        getPlayer
      }

    })
      .then((res) => console.log(res))
      .catch((err) => console.error(err));
  };

  return (
    <div className="content">
      <h1>Find and Add a Player</h1>

      <div id="cent">
        <form style={{ justifyContent: "flex-start" }}>
          <label>
            Player Name:
            <input
              type="text"
              name="activityName"
              onChange={(e) => {
                setSearchedPlayerName(e.target.value);
              }}
            />
          </label>
          <button
            onClick={(e) => {
              searchPlayer();
              e.preventDefault();
            }}
            style={{ marginLeft: 85, backgroundColor: "black" }}
          >
            Search
          </button>
        </form>

        <div style={{ alignItems: "center" }}>
          <Table style={{ marginLeft: "auto", marginRight: "auto" }}>
            <thead>
              <tr>
                <th>Id</th>
                <th>Player Name</th>
                <th>Add</th>
              </tr>
            </thead>
            <tbody>
              <td>{getPlayer?.id}</td>
              <td>{getPlayer?.name}</td>
              <td>
                <Button
                  onClick={() => {addToTeam()}}
                  style={{ background: "black", width: 50 }}
                >
                  Add
                </Button>
              </td>
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
};

export default FindPlayerView;
