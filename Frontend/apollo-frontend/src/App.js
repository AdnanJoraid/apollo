import React from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import HomeView from "./Views/HomeView";
import UserHomeView from "./Views/UserContent/UserHomeView";
import UserActivityListView from "./Views/UserContent/UserActivityListView";
import UserJoinActivity from "./Views/UserContent/UserJoinActivity";
import UserChatView from "./Views/UserContent/UserChatView";
import TeamHomeView from "./Views/TeamContent/TeamHomeView";
import FindPlayerView from "./Views/TeamContent/FindPlayerView";
import ChatTeamView from "./Views/TeamContent/ChatTeamView";
import BookTeamActivityView from "./Views/TeamContent/BookTeamActivityView";
import OrganizationHome from "./Views/OrganizationContent/OrganizationHome";
import OrganizationAddPlayer from "./Views/OrganizationContent/OrganizationAddPlayer";
import OrganizationAddTeam from "./Views/OrganizationContent/OrganizationAddTeam"
function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<HomeView />} />
        <Route path="/userHome" element={<UserHomeView />} />
        <Route path="/userActivityList" element={<UserActivityListView />} />
        <Route path="/userJoinActivity" element={<UserJoinActivity />} />
        <Route path="/organizationHome" element={<OrganizationHome />} />
        <Route path="/organizationAddPlayer" element={<OrganizationAddPlayer />} />
        <Route path="/organizationAddTeam" element={<OrganizationAddTeam />} />
        <Route path="/userChat" element={<UserChatView />} />
        <Route path="/TeamHome" element={<TeamHomeView />} />
        <Route path="/TeamChat" element={<ChatTeamView />} />
        <Route path="/FindPlayer" element={<FindPlayerView />} />
        <Route path="/BookTeamActivity" element={<BookTeamActivityView />} />
      </Routes>
    </Router>
  );
}

export default App;
