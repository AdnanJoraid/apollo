using System;
using System.Collections.Generic;

namespace ApolloAPI.Backend.Models
{
    public class Team
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        
        public List<Player> Players { get; set; }

    }

}