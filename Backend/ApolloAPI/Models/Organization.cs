using System;
using System.Collections.Generic;

namespace ApolloAPI.Backend.Models
{
    public class Organization
    {
        public Guid Id { get; set; }
        public String Name { get; set; }

    }

}