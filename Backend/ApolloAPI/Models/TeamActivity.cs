using System;
using System.Collections.Generic;
using ApolloAPI.Backend.Models;
namespace ApolloAPI.Backend.Models
{
    public class TeamActivty : Activty 
    {
        public Guid Id { get; set; }
        public Team HomeTeam {get; set;}

        public Team AwayTeam { get; set; }

    }

}