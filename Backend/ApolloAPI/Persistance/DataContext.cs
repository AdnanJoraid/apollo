using System;
using Microsoft.EntityFrameworkCore;
using ApolloAPI.Backend.Models; 

namespace ApolloAPI.Persistance
{
    public class DataContext : DbContext

    {

        public DbSet<Player> Players {get; set;}
        public DbSet<Team> Teams {get; set;}
        public DbSet<Organization> Organizations {get; set;}
        public DbSet<PlayerActivty> PlayerActivties {get; set;}
        public DbSet<TeamActivty> TeamActivties {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){

        optionsBuilder.UseSqlite("filename=ApolloDb.db");

        }

    }

}