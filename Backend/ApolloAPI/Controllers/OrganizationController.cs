using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApolloAPI.Persistance;
using Microsoft.Extensions.Logging;
using ApolloAPI.Backend.Models;
using Microsoft.EntityFrameworkCore;
namespace ApolloAPI.Controllers
{
   
    [ApiController]
    [Route("api/[controller]")]
    public class OrganizationController : Controller
    {

        private readonly DataContext _context;

        public OrganizationController(DataContext context)
        {
            _context = context;
        }

        [HttpPost("addorganization")]
        public async Task<IActionResult> AddOrganization([FromBody] Organization organization)
        {
            try{
                if(organization == null)
                    BadRequest("No organization given");
                
                var db = _context.Organizations;
                await db.AddAsync(organization);
                await _context.SaveChangesAsync();
                return Ok(organization);


            }catch (Exception e){
                return BadRequest(e.ToString());
            }
        }


        [HttpPost("addplayer")]
        public async Task<IActionResult> PostPlayerActivity([FromBody] PlayerActivty model)
        {
            try
            {
                if (model == null)
                    BadRequest("No Activity give");

                var db = _context.PlayerActivties;
                var search = await db.FirstOrDefaultAsync(x => x.Id.Equals(model.Id));

                await db.AddAsync(model);
                await _context.SaveChangesAsync();
                return Ok(model);

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }


        }

        [HttpPost("addteam")]
        public async Task<IActionResult> PostTeamActivity([FromBody] TeamActivty teamActivity)
        {
            try
            {

                if (teamActivity == null)
                {
                    return BadRequest("Activity is empty");
                }
                var db = _context.TeamActivties;


                await db.AddAsync(teamActivity);
                await _context.SaveChangesAsync();
                return Json($"Team Activity with Id {teamActivity.Id} has been added");

            }
            catch (Exception e)
            {
                return Json(e.ToString());
            }
        }


    }
}
