﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ApolloAPI.Backend.Models;
using ApolloAPI.Persistance; 
namespace ApolloAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamController : Controller
    {

        private readonly DataContext _context;

        public TeamController(DataContext context)
        {
            _context = context;
        }

        [HttpPost("addteam")]
        public async Task<IActionResult> AddTeamToDb([FromBody] Team team)
        {
            try {

                if(team == null)
                    return BadRequest("Team is empty"); 


                var db = _context.Teams; 

                await db.AddAsync(team); 
                await _context.SaveChangesAsync();
                return Ok($"Team {team.Id} has been added to database"); 

            }catch (Exception e){
                return BadRequest(e.ToString()); 
            }
        }


        [HttpPost("addplayer/{id}")]
        public async Task<IActionResult> AddPlayerToTeam([FromBody] Player player, string id)
        {
            try {
                var db = _context.Teams.Include(x => x.Players).FirstOrDefault(x => x.Id.Equals(new Guid(id)));

                if (db != null){
                    db.Players.Add(player);
                    await _context.SaveChangesAsync();
                    return Ok($"Player {player.Name} has been added to Team {db.Name} with id {id}"); 
                }

                return BadRequest("Team Doesn't exist"); 

            }catch(Exception e){
                return BadRequest(e.ToString());
            }

        }

        [HttpPost("addactivity/{name}")]
        public async Task<IActionResult> PostTeamActivity([FromBody]  TeamActivty teamActivty, string name)
        {
            if (teamActivty == null || name == null)
                return BadRequest("One or all of the arguments passed is null");
 
                    
            try {
                var teamDb = _context.Teams.FirstOrDefault(x => x.Name.Equals(name));
                var db = _context.TeamActivties;
                if (teamDb != null){
                    teamActivty.AwayTeam = teamDb;
                    await db.AddAsync(teamActivty);
                    await _context.SaveChangesAsync();
                    return Ok("Team activity has been added");
                }
                return BadRequest("Error. Data already exist"); 
                

            }catch (Exception e){
                return BadRequest(e.ToString()); 
            }
         

        }
    }
}
