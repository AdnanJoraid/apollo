using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ApolloAPI.Backend.Models;
using ApolloAPI.Persistance;
namespace ApolloAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PlayerController : Controller
    {
        
        private readonly DataContext _context;

        public PlayerController(DataContext context)
        {
            _context = context;
        }

        [HttpPost("addplayerdb")]
        public async Task<IActionResult> AddPlayerToDB([FromBody] Player player)
        {
            try{
                if(player == null)
                    return BadRequest("No Player Given");

                var db = _context.Players;
                await db.AddAsync(player);
                await _context.SaveChangesAsync();
                return Ok(player);

            } catch(Exception e){
                return BadRequest(e.ToString());
            }
        }

        [HttpPost("{id}/addplayer")]
        public async Task<IActionResult> AddPlayerToActivity(string id,[FromBody] Player player)
        {
            //209dca0c-1af6-4cc4-919f-01de9e4be1d1 - Player Id
            //470F1FFB-73BF-4002-B78F-D4685ECBCFE6 - Activity Id
            try{
                if(player == null)
                    return BadRequest("No player given");

                var activity = await _context.PlayerActivties.Include(x =>x.Players).SingleOrDefaultAsync(x=>x.Id.Equals(new Guid(id)));
                // var s =  _context.PlayerActivties.SingleOrDefault((x => x.Id.Equals(new Guid(id))));
                
                // var query = from act in _context.PlayerActivties where player.Id.Equals(id) select act;
                
                if(activity == null)
                    return BadRequest($"No activity with ID {id} found");
                activity.Players.Add(player);
                await _context.SaveChangesAsync();
                return Ok(activity);

            }catch(Exception e){
                return BadRequest(e.ToString());
            }
            
        }

        [HttpGet("getActivites")]
        public async Task<IActionResult> GetAllActivities()
        {
            var activities = _context.PlayerActivties.Include(x =>x.Players);

            return Ok(activities);
        }

        [HttpGet("getplayer/{name}")]
        public async Task<IActionResult> GetPlayerByName(string name)
        {
            try
            {

                var dbPlayer = await _context.Players.SingleOrDefaultAsync(x => x.Name.Equals(name));

                if (dbPlayer == null)
                    return BadRequest("player doesn't exist");

                return Ok(dbPlayer);

            }
            catch (Exception e)
            {
                return Json(e.ToString());
            }
        }
    }
}
